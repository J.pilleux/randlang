#include <stdio.h>
#include <stdlib.h>

void release_array(void** array) {
    for (long unsigned int i = 0; i < sizeof(array) - 1; ++i) {
        free(array[i]);
    }
    free(array);
}

char* get_random_choice(char** array) {
    int len = sizeof(array);
    int rand_index = rand() % (len - 1);
    char* choice = array[rand_index];
    if (choice[0] == 0) {
        return get_random_choice(array);
    }
    return choice;
}
