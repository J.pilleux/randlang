#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#define MAX_LANG_SIZE 32

static int get_file_line_number(const char* filename) {
    int line_count = 0;
    FILE* file = fopen(filename, "r");

    if (file == NULL) {
        fprintf(stderr, "\033[31mError: \033[0mCannot open file %s\n", filename);
    }

    char c;
    while ((c = fgetc(file)) != EOF) {
        if (c == '\n') {
            ++line_count;
        }
    }
    fclose(file);
    return line_count;
}

char** read_file(const char* filename) {
    int line_count = get_file_line_number(filename);
    char** file_content = (char**)malloc(line_count * sizeof(char*));
    for (int i = 0; i < line_count; ++i) {
        file_content[i] = (char*)malloc(MAX_LANG_SIZE * sizeof(char));
    }
    char buf[MAX_LANG_SIZE];

    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        fprintf(stderr, "\033[31mError: \033[0mCannot open file %s\n", filename);
    }

    int index = 0;
    while (fscanf(file, "%[^\n] ", buf) != EOF) {
        strcpy(file_content[index++], buf);
    }
    fclose(file);
    return file_content;
}
