#include "src/array.h"
#include "src/file.h"
#include "src/display.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef DEBUG
#define FILENAME "./progs"
#else
#define FILENAME "/etc/randlang/progs"
#endif

int main() {
    srand(time(NULL));
    char** content = read_file(FILENAME);
    display_choice(content);
    release_array((void**)content);
    return EXIT_SUCCESS;
}
