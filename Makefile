CC = gcc
CFLAGS = -Wall -Wextra -Wpedantic
DEBUG =
LDFLAGS = -lm
RM = rm -f

SRC = randlang.c src/file.c src/array.c src/display.c
TARGET = randlang
CONF = progs

PREFIX=/usr/bin
CONF_PREFIX=/etc/randlang

all: $(TARGET)

debug: DEBUG = -DDEBUG -g
debug: all

$(TARGET): $(SRC)
	$(CC) $(CFLAGS) $(DEBUG) $(LDFLAGS) -o $@ $^

clean:
	$(RM) $(TARGET) *.o

install: install_bin install_conf

install_bin: $(TARGET)
	cp -f $(TARGET) $(PREFIX)/$(TARGET)

install_conf:
	mkdir -p $(CONF_PREFIX)
	cp -f $(CONF) $(CONF_PREFIX)/$(CONF)

uninstall:
	rm -f $(PREFIX)/$(TARGET)
	rm -rf $(CONF_PREFIX)/$(CONF)
