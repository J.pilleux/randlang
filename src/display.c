#include "array.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

static char* append(char* str1, char* str2) {
    char* new_str = (char*) malloc(strlen(str1) + strlen(str2));
    new_str[0] = '\0';
    if (new_str != NULL) {
        strcat(new_str, str1);
        strcat(new_str, str2);
    }
    return new_str;
}

static void cell_display(char* string) {
    size_t len = strlen(string);
    char* top = "+";
    for (size_t i = 0; i <= len + 1; ++i) {
        top = append(top, "-");
    }
    printf("\033[A\33[2KT\r %s+\n", top);
    printf(" | %s |\n", string);
    printf(" %s+\n", top);
}

void display_choice(char** langs) {
    char* msg;
    for (double i = 0.05; i < 0.058; i += 0.0001) {
        msg = get_random_choice(langs);
        printf("\033[A\33[2KT\r> %s\n", msg);
        fflush(stdout);
        int time_to_sleep = (int)((log(i) + 3) * 1000000);
        usleep(time_to_sleep); // This is in microseconds
    }
    cell_display(msg);
}
