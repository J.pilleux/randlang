import System.Random (randomRIO)

readProgs :: FilePath -> IO [String]
readProgs = fmap lines . readFile

pickProg :: [a] -> IO a
pickProg elem = fmap (elem !!) $ randomRIO (0, length elem -1)

main = do
    content <- readProgs "./progs"
    prog <- pickProg cC
    putStr prog
